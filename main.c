//~~~Bibliotheken~~~
#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "canbus.pio.h"


//~~~globale Variable~~~
static const uint led_pin = 25;
static const uint tx = 21; 
static const float pio_freq_div = 6.65;                             // 133Mhz / 6.65 = 20Mhz  
static const uint16_t Polynom = 0b1100010110011001;                 // Prüfpolynom
static const uint16_t ID = 0b011111111000;                          // 2040 für Raspberry Pi Pico 2040 12 Bits von MSB sowie 0 als Startbit

//~~~Vordfinition Funktion~~~                
uint16_t search_first_One(uint16_t *CRC_Frame);
uint16_t CRC_Berechnung (uint *Firstframe, uint *Data_Frame_0,uint *Data_Frame_1, uint8_t DataLength_of_Data);
void send_Frame();

//~~~main~~~
int main() {

    //---Grundfunktionen---
    stdio_init_all();                                                   // Serialmonitor wird initialisiert 
    
    //---PIO Einstellungen---
    PIO pio = pio0;                                                     // Instanz von der PIO
    uint sm = pio_claim_unused_sm(pio, true);                           // gibt die erste freie Maschine wieder in der Pio0
    uint offset = pio_add_program(pio, &canbus_program);                // gibt den Offset des Speichers zurück 
    canbus_program_init(pio, sm, offset, tx, pio_freq_div);             // Initialisierung der Pio
     
    pio_sm_set_enabled(pio, sm, true);                                  // Programm wird gestartet
        
    uint8_t _1 = 0b00111000;
    uint8_t _2 = 0b00000111;
    uint8_t _3 = 0b00111000;

    uint8_t arry[] = {_1, _2, _3};
    
    //---Schleife---
    while (true) {
        sleep_ms(2000);
        send_Frame(pio, sm, 3, arry);
        
    }
}

/// @brief Die Funktion sendet den CAN_Frame an die Pio weiter
/// @param pio Zuständige Pio
/// @param sm freie Status Mschine
/// @param DataFrame_Length Länge des Datenframes
/// @param Data_Arr Datenframe
void send_Frame(PIO pio, uint sm, uint DataFrame_Length, uint8_t Data_Arr[]){
    uint16_t CRC = 0;
    uint counter_bit = 0;
  
    //---Lokale Vaiable---
    uint row_Firstframe = 0;                // Startbit, ID, RTD, IDE, re0, DLC
    uint row_Data_Frame_0 = 0;              // Dataframe 0.-4. Byte
    uint row_Data_Frame_1 = 0;              // Dataframe 5.-8. Byte
   
    row_Firstframe = ID;                    // Startbit 0 ist Vorhanden, + ID
    row_Firstframe <<=20;                   // Einfügen 20 Nullen damit steht die erste Null auf Platz 31!
    //row_Firstframe = 0b000 << 17;         // RTR (0), IDE(0), re0 (0); nicht nötig zu verändern!
    int Verschriebung_Length = 13;          // Position für die Länge des DatenFrames
    
    //---Länge des Datenframs & Datenarray---
    switch(DataFrame_Length){               // befüllen der Hilfsvariablen              
    case 0:
        row_Firstframe ^= 0b0000 << Verschriebung_Length;
    break;
    case 1:
        row_Firstframe ^= 0b0001 << Verschriebung_Length;
        row_Data_Frame_0 ^= Data_Arr[0] << 24;
    break;
    case 2:
        row_Firstframe ^= 0b0010 << Verschriebung_Length;
        row_Data_Frame_0 ^= Data_Arr[0] << 24;
        row_Data_Frame_0 ^= Data_Arr[1] << 16;
    break;
    case 3:
        row_Firstframe ^= 0b0011 << Verschriebung_Length;
        row_Data_Frame_0 ^= Data_Arr[0] << 24;
        row_Data_Frame_0 ^= Data_Arr[1] << 16;
        row_Data_Frame_0 ^= Data_Arr[2] << 8;
    break;
    case 4:
        row_Firstframe ^= 0b0100 << Verschriebung_Length;
        row_Data_Frame_0 ^= Data_Arr[0] << 24;
        row_Data_Frame_0 ^= Data_Arr[1] << 16;
        row_Data_Frame_0 ^= Data_Arr[2] << 8;
        row_Data_Frame_0 ^= Data_Arr[3] << 0;
    break;
    case 5:
        row_Firstframe ^= 0b0101 << Verschriebung_Length;
        row_Data_Frame_0 ^= Data_Arr[0] << 24;
        row_Data_Frame_0 ^= Data_Arr[1] << 16;
        row_Data_Frame_0 ^= Data_Arr[2] << 8;
        row_Data_Frame_0 ^= Data_Arr[3] << 0;
        row_Data_Frame_1 ^= Data_Arr[4] << 24;
    break;
    case 6:
        row_Firstframe ^= 0b0110 << Verschriebung_Length;
        row_Data_Frame_0 ^= Data_Arr[0] << 24;
        row_Data_Frame_0 ^= Data_Arr[1] << 16;
        row_Data_Frame_0 ^= Data_Arr[2] << 8;
        row_Data_Frame_0 ^= Data_Arr[3] << 0;
        row_Data_Frame_1 ^= Data_Arr[4] << 24;
        row_Data_Frame_1 ^= Data_Arr[5] << 16;
    break;
    case 7:
        row_Firstframe ^= 0b0111 << Verschriebung_Length;
                row_Data_Frame_0 ^= Data_Arr[0] << 24;
        row_Data_Frame_0 ^= Data_Arr[1] << 16;
        row_Data_Frame_0 ^= Data_Arr[2] << 8;
        row_Data_Frame_0 ^= Data_Arr[3] << 0;
        row_Data_Frame_1 ^= Data_Arr[4] << 24;
        row_Data_Frame_1 ^= Data_Arr[5] << 16;
        row_Data_Frame_1 ^= Data_Arr[6] << 8;
    break;
    case 8:
        row_Firstframe ^= 0b1000 << Verschriebung_Length;
        row_Data_Frame_0 ^= Data_Arr[0] << 24;
        row_Data_Frame_0 ^= Data_Arr[1] << 16;
        row_Data_Frame_0 ^= Data_Arr[2] << 8;
        row_Data_Frame_0 ^= Data_Arr[3] << 0;
        row_Data_Frame_1 ^= Data_Arr[4] << 24;
        row_Data_Frame_1 ^= Data_Arr[5] << 16;
        row_Data_Frame_1 ^= Data_Arr[6] << 8;
        row_Data_Frame_1 ^= Data_Arr[7] << 0;
    break;
    }

    //---Lokale Variable---    
    int offset = 0;                         // Indikator Häufigkeit der 0 oder 1 bei 5 Gleichnamigen wird zwischen 4 und 5 ein Endgegengesetztes Signal gesetzt.
    bool akkVal = 0;                        // Boolean Wert für 0(false) und 1(true)
    int pos = 0;                            // Dienst zur Array Position
    //---Sende Variable---
    uint Sendeframe_0 = 0;                  
    uint Sendeframe_1 = 0;
    uint Sendeframe_2 = 0;
    uint Sendeframe_3 = 0;
    uint Sendeframe_4 = 0; 
    //---Sende Array---
    uint Sendeframe[] = {Sendeframe_0, Sendeframe_1, Sendeframe_2, Sendeframe_3, Sendeframe_4};
    uint *pts = Sendeframe;                 // Pointer auf Array, Array als solches ist auch ein Pointer, jedoch gibt es Probleme im Verlauf
    
    //---CRC Berechnung--   
    CRC = CRC_Berechnung(&row_Firstframe, &row_Data_Frame_0, &row_Data_Frame_1, DataFrame_Length); 
    
    //---Bestückung des Sendeframes---      Eine Funktion würde Abhilfe schaffen, jedoch ohne Debuger ist es schwer!       
    for(int i = 0; i<19; i++){
        // Es werden Stuffenbits gesetzt und zusammen mit den Daten im Sendeframe eingefügt
        akkVal = ((row_Firstframe & (1<< 31-i)) != 0)? true: false;
            if(akkVal){
                if (offset < 5){
                    if(offset == -5){
                        if(counter_bit== 32){pos+=1; counter_bit = 0;}
                            *(pts+pos)<<=1; *(pts+pos) ^= 1<<0; offset = 0; ++counter_bit;
                        }
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1; *(pts+pos) ^= 1<<0; 
                    if(offset <=0){offset = 1;}else {offset++;}}
                else if(offset == 5){
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1;
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    *(pts+pos)<<=1;*(pts+pos) ^= 1<<0; 
                    ++counter_bit; offset = 1;
                } 
            }else{
                if (offset > -5){
                    if(offset == -5){
                        if(counter_bit== 32){pos+=1; counter_bit = 0;}
                            *(pts+pos)<<=1; offset = 0; ++counter_bit;
                        }
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1; 
                    if(offset >=0){offset = -1;}else {offset--;}}
                else if(offset == -5){
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1;*(pts+pos) ^= 1<<0;
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    *(pts + pos) <<= 1;
                    ++counter_bit;offset = -1;
                }
            }  
    }
    
    //---Variable für Dataframe---
    int Data_counter_0 =0;
    int Data_counter_1 =0;
    //---Bestückung und Aufteilung der Variablen
    if(DataFrame_Length<5){Data_counter_0 = 8*DataFrame_Length;} 
    else if(DataFrame_Length == 4){Data_counter_0 = 32;}
    else if(DataFrame_Length > 4){Data_counter_0 = 32; Data_counter_1 = 8*(DataFrame_Length-4);}
    //---Bestückung des Sendeframes, Dataframe---
    for(int i = 0; i<Data_counter_0; i++){
        akkVal = ((row_Data_Frame_0 & (1<< 31-i)) != 0)? true: false;                   // Es wird ein Boolean Wert erzeugt (true, 1 : false, 0)
            if(akkVal){
                if (offset < 5){
                    if(offset == -5){                                                   // Wenn davor 5 Nullen stehen, wird der Invertierte Wert erzeugt
                        if(counter_bit== 32){pos+=1; counter_bit = 0;}              // Abfrage ob noch Platz in der Variable ist (Wertebereich 1-32!) pos+1 und counter_bit auf 0
                            *(pts+pos)<<=1; *(pts+pos) ^= 1<<0; offset = 0;             // Eine Null wird von rechts eingefügt und diese getriggert und der Offset wird auf 0 gesetzt
                            ++counter_bit;}                        
                    if(counter_bit == 32){pos+=1; counter_bit = 0;}                 // s.Oben
                    ++counter_bit; *(pts+pos)<<=1; *(pts+pos) ^= 1<<0;                  // counterbit+1 (Vor Benutzung) Einsetzten eine Null von rechr und diese mit XOR Triggern 
                    if(offset <=0){offset = 1;}else {offset++;}}
                else if(offset == 5){                                                   // bei der 6. null wird zuvor noch ein Invertierter Wert gesetzt
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}                 // s.Oben
                    ++counter_bit; *(pts+pos)<<=1;                                      // eine Null wird von recht eingeschoben
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    *(pts+pos)<<=1;*(pts+pos) ^= 1<<0; 
                    ++counter_bit; offset = 1;
                } 
            }else{                                                                      // equivalent zu Oben jedoch mit der Null false
                if (offset > -5){
                    if(offset == -5){
                        if(counter_bit== 32){pos+=1; counter_bit = 0;}
                            *(pts+pos)<<=1;  offset = 0; ++counter_bit;
                        }
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1; 
                    if(offset >=0){offset = -1;}else {offset--;}}
                else if(offset == -5){
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1;*(pts+pos) ^= 1<<0;
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    *(pts + pos) <<= 1;
                    ++counter_bit;offset = -1;
                }
            }  
    } 
    //---Bestückung des Sendeframes, Dataframe---
    if(DataFrame_Length>4){
        for(int i = 0; i<Data_counter_1; i++){
        
            akkVal = ((row_Data_Frame_1 & (1<< 31-i)) != 0)? true: false;
            if(akkVal){
                if (offset < 5){
                    if(offset == -5){
                        if(counter_bit== 32){pos+=1; counter_bit = 0;}
                            *(pts+pos)<<=1; *(pts+pos) ^= 1<<0; offset = 0;
                            ++counter_bit;
                        }
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1; *(pts+pos) ^= 1<<0; 
                    if(offset <=0){offset = 1;}else {offset++;}}
                else if(offset == 5){
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1;
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    *(pts+pos)<<=1;*(pts+pos) ^= 1<<0; 
                    ++counter_bit; offset = 1;
                } 
            }else{
                if (offset > -5){
                    if(offset == -5){
                        if(counter_bit== 32){pos+=1; counter_bit = 0;}
                            *(pts+pos)<<=1; offset = 0;
                            ++counter_bit;
                        }
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1; 
                    if(offset >=0){offset = -1;}else {offset--;}}
                else if(offset == -5){
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1;*(pts+pos) ^= 1<<0;
                    if(counter_bit== 32){pos+=1; counter_bit = 0;}
                    *(pts + pos) <<= 1;
                    ++counter_bit;offset = -1;
                }
            }  
        }
    }
   
    for (int i = 1; i < 16; i++)
    {
        akkVal = ((CRC & (1<< 15-i)) != 0)? true: false;
            if(akkVal){
                if (offset < 5){
                    if(offset == -5){
                        if(counter_bit== 32){++pos; counter_bit = 0;}
                            *(pts+pos)<<=1; *(pts+pos) ^= 1<<0; offset = -1;
                                ++counter_bit;
                             }             
                              
                    if(counter_bit== 32){++pos; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1; *(pts+pos) ^= 1<<0; 
                    if(offset <=0){offset = 1;}else {++offset;}
                                      
                    }
                else if(offset == 5){
                    if(counter_bit== 32){++pos; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1;
                    if(counter_bit== 32){++pos; counter_bit = 0;}
                    *(pts+pos)<<=1;*(pts+pos) ^= 1<<0; 
                    ++counter_bit; offset = 0;
                } 
            }else{
                if (offset > -5){
                    if(offset == 5){
                        if(counter_bit== 32){++pos; counter_bit = 0;}
                            *(pts+pos)<<=1; offset = -1; ++counter_bit;

                        }
                    if(counter_bit== 32){++pos; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1; 
                    if(offset >=0){offset = -1;}else {--offset;}}
                else if(offset == -5){
                    if(counter_bit== 32){++pos; counter_bit = 0;}
                    ++counter_bit; *(pts+pos)<<=1; *(pts+pos) ^= 1<<0;
                    if(counter_bit== 32){++pos; counter_bit = 0;}
                    *(pts+pos) <<= 1;
                    ++counter_bit; offset = 0;
                }
            }
        }
    
    //---CRC Begrenzer---
    if (counter_bit==32){++pos; counter_bit = 0;}
    *(pts+pos) <<=1;
    *(pts+pos) ^= 1<<0;
    ++counter_bit;
     
    //---ACK-Slot---
    if (counter_bit==32){++pos; counter_bit = 0;}
    *(pts+pos) <<=1;
    ++counter_bit;
    if (counter_bit==32){++pos; counter_bit = 0;}
    *(pts+pos) <<=1;
    *(pts+pos) ^= 1<<0;
    ++counter_bit;
    
    //---END OF FRAME---
    for (int i = 0; i < 7; i++)
    {
        if (counter_bit==32){++pos; counter_bit = 0;}
        *(pts+pos) <<=1;
        *(pts+pos) |= 1<<0;
        ++counter_bit;
    }
    
    //---Restlichen Bits mit 0 Auffüllen
    for (int i = 0; i < (32-counter_bit); i++)
    {
        *(pts+pos) <<=1;
        if(i>2){*(pts+pos) ^=1<<0;}
    }
    
   for(int i = 0; i <=pos;i++){
        pio_sm_put_blocking(pio, sm, Sendeframe[i]);
    }
 }


/// @brief Berechnet die Prüfsumme und gibt diese zurück
/// @param Firstframe 19 erste Framebits
/// @param Data_Frame_0 32 Dataframe_0 die mit max. 4*8 Byts befüllt wurden sind
/// @param Data_frame_1 32 Dataframe_1 die mit max. 4*8 Byts befüllt wurden sind
/// @param Length_of_Data Länge des Datenframes
/// @return gibt die Prüfsumme wieder
uint16_t CRC_Berechnung(uint *Firstframe, uint *Data_Frame_0, uint *Data_Frame_1, uint8_t Length_of_Data){
    // Firstframe 19 Lang
    //---Variable---
    uint16_t CRC = 0;                                       // Valiable für die Prüfsumme
    uint abb_Val_Data_0 = 0;                                // Schleifenabbruchkriterium für den Ersten Dataframe
    uint abb_Val_Data_1 = 0;                                // Schleifenabbruchkriterium für den Ersten Dataframe
    uint position = 0;                                      // Positions erste 1. in CRC; 
    int step = 0;                                           // Unterschied zwischen (true)Firstframe und (false)Datenframe
    int counter = 16;                                       // zuständig für Position im Firstframe 16 Bits wurden vorab verschoben
    bool Schleife_Continue = false;                         // Noch offen Stellen in CRC;
    //---erste 16 Bits ins CRC laden---
    for (int i = 0; i <16;i++){                             
       CRC |= ((*Firstframe & 1<< 31-i)? 1: 0)<<15-i;       // Bits werden mit Hilfe einer Maske gefüllt
    }
    
    //---Abbruchkriterium defeniert---
    switch (Length_of_Data){
        case 0:
            break;
        case 1:
            abb_Val_Data_0 = 8*1; 
            break;
        case 2:
            abb_Val_Data_0 = 8*2;
            break;
        case 3:
            abb_Val_Data_0 = 8*3;
            break;
        case 4:
            abb_Val_Data_0 = 8*4-1;
            break;    
        case 5:
            abb_Val_Data_0 = 8*4-1;
            abb_Val_Data_1 = 8*1;
            break;    
        case 6:
            abb_Val_Data_0 = 8*4-1;
            abb_Val_Data_1 = 8*2;
            break;    
        case 7:
            abb_Val_Data_0 = 8*4-1;
            abb_Val_Data_1 = 8*3;
            break;    
        case 8:
            abb_Val_Data_0 = 8*4-1;
            abb_Val_Data_1 = 8*4-1;
            break; 
    }
    //---Berechnung der CRC-Summe---
    while(1){
        position = search_first_One(&CRC);                              // suche nach der ersten 1 MSB
        switch (step)
        {
        case 0:                                                         // Bits werden aus Firstframe nach bedarf entnommen
            for (int i = 0; i < position; i++)
                {
                    CRC <<= 1;                                          // Einfugen einer 0 von rechts (LSB)
                    CRC |= ((*Firstframe & 1<< 31-counter++)? 1: 0)<<0; // Der LSB wird gesetzt
                }    
            CRC = CRC ^ Polynom;                                        // XOR verknüpfung
            if (counter == 19){step = 3; }                              // Prüfen ob die 19 Stelle ereicht ist.
            break;
        case 1:                                                         // befüllen aus dem Datenframe 0
            for (int i = 0; i < position; i++)
            {                
                CRC <<= 1;                                              // Einfugen einer 0 von rechts (LSB) 
                CRC |= ((*Data_Frame_0 & 1<< 31-counter++)? 1: 0)<<0;   // Der LSB wird gesetzt
                if (counter == abb_Val_Data_0 && i>0)                   // Prüft auf Ausreichend Bits im Frame sonst abbruch
                    {step = 2; break; Schleife_Continue = true;}        // Wenn nicht ausreichend Bits vorhanden sind wird der Schleifen_Continue werte auf True gesetzt
            }
            if (!Schleife_Continue){CRC ^= Polynom; Schleife_Continue = false;}  // XOR Verknüpfung / Wenn nicht ausreichend Bits gesetzt sind wird wird keine XOR Verknüpfung durchgeführt
            if (Length_of_Data <4){                                     // Unterscheidung ob das zweites Datenarray Vorhanden ist
                if (counter == abb_Val_Data_0){step = 4; }              // Prüft auf Abbruchkreterium und Springt zum nächsten Schritt
            }else{if((counter-1) == abb_Val_Data_0){step = 2; counter = 0; }                 
            break;
        case 2:
            for (int i = 0; i < position; i++)
            {                
                CRC <<= 1;                                              // Einfugen einer 0 von rechts (LSB) 
                CRC |= ((*Data_Frame_1 & 1<< 31-counter++)? 1: 0)<<0;   // neue Stellen werden entsprechend gesetzt, immer die erste von rechts & Counter nach Benutzung dekrementiert.
                if (counter == abb_Val_Data_1 && i>0)                   // Der LSB wird gesetzt
                    {step = 2; break; Schleife_Continue = true;}        // Prüft auf Ausreichend Bits im Frame sonst abbruch
            }
            if (!Schleife_Continue){CRC ^= Polynom; Schleife_Continue = false;} // XOR Verknüpfung / Wenn nicht ausreichend Bits gesetzt sind wird wird keine XOR Verknüpfung durchgeführt
            if ( (counter-1) == abb_Val_Data_1){step = 4;}           
            }                 
            break;
        case 3:                                                         // Vorbereitung für Dataframe
            counter = 0; step = 1;
            break;
        case 4:                                                         // Vorbereitung für die CRC 15 Nullen
            counter = 0;
            step = 5;
            break;
        case 5:
            for (int i = 0; i < position; i++)                          
            {
                CRC <<= 1;
                ++counter;                
            }
            CRC ^= Polynom;
            if (counter == 15){return CRC;}
            break;
        default:
            // ERROR?                                                   // Hat keine Aufgaben, ist für Erweiterungen vorgesehen
            break;
        }
    }
    //---Dieses Ende wird nie erreicht, wegen while(1)!---  
      
}

/// @brief sucht nach der ersten 1 im Frame und gibt die Positions zurück; Als ERROR ist die Null vorgesehen
/// @param CRC_Frame aktuelle Prüfsummenstand
/// @return gibt die Position zurück

uint16_t search_first_One(uint16_t *CRC_Frame){
    for (uint16_t i = 0;i<16;i++){                                     // Unter Verwendung einer Maske wird CRC durchlaufen und die Stellen bis zur ersten 1 von MSB gezählt und diesen werden zurückgegeben
        if((*CRC_Frame & (1<<15-i)) > 0) {return i;}
    }
    return 20;                                                          // Wenn Fehler auftretten
}    
